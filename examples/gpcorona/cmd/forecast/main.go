package main

import (
	. "bitbucket.org/dtolpin/abda-course/examples/gpcorona/common"
	. "bitbucket.org/dtolpin/abda-course/examples/gpcorona/kernel/ad"
	"bitbucket.org/dtolpin/gogp/gp"
	"bitbucket.org/dtolpin/gogp/kernel/ad"
	"bitbucket.org/dtolpin/infergo/ad"
	"encoding/csv"
	"flag"
	"log"
	"math"
	"os"
)

func main() {
	flag.Parse()

	// Check arguments
	if flag.NArg() > 0 {
		log.Fatalf("unexpected positional arguments: %v", flag.Args())
	}

	if PARALLEL {
		ad.MTSafeOn()
	}

	// Read the data
	header, X, Y := Read(csv.NewReader(os.Stdin))

	// Optionally log-transform output
	if LOG {
		for i := range Y {
			Y[i] = math.Log(Y[i])
		}
	}

	// standardize output
	s, s2, n := 0., 0., 0.
	for i := range Y {
		s += Y[i]
		s2 += Y[i] * Y[i]
		n++
	}
	shift := s / n
	scale := math.Sqrt(s2/n - shift*shift)
	for i := range Y {
		Y[i] = (Y[i] - shift) / scale
	}

	// Train GP
	gp := &gp.GP{
		NDim:     1,
		Noise:    kernel.UniformNoise,
		Parallel: ad.IsMTSafe(),
		X:        X,
		Y:        Y,
	}
	if SEASONAL {
		gp.Simil = &ARS{}
	} else {
		gp.Simil = &AR{}
	}
	Fit(gp)
	if !QUIET {
		log.Printf("GP hyperparameters: %f\n",
			append(gp.ThetaSimil, gp.ThetaNoise...))
	}

	// forecast
	time := X[len(X)-1][0]
	for i := 0; i != NSTEPS; i++ {
		time += STEP
		X = append(X, []float64{time})
		Y = append(Y, math.NaN())
	}
	mu, sigma, err := gp.Produce(X)
	if err != nil {
		log.Fatalf("Cannot forecast: %v", err)
	}
	// Add inferred uniform noise to predictions
	for i := range sigma {
		sigma[i] += gp.ThetaNoise[0]
	}
	// Restore the output to the original scale
	for i := range X {
		Y[i] = Y[i]*scale + shift
		if LOG {
			Y[i] = math.Exp(Y[i])
		}
	}
	Write(csv.NewWriter(os.Stdout),
		header, X, Y, mu, sigma, shift, scale)
}
