module bitbucket.org/dtolpin/abda-course/examples/gpcorona

go 1.14

require (
	bitbucket.org/dtolpin/gogp v0.3.1
	bitbucket.org/dtolpin/infergo v0.8.4
)
