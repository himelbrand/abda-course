# Assignment 3: Model checking and evaluation

This assignment is dedicated to chapters 6 and 7 of the book.
Submission deadline is Tue, June 23, 2020, 23:59 AOE.

This assignment contains two programming questions based on
models from Assignment 2 and a bonus theoretical question (+40
points to assignment (5 points to the final grade).  You are not
required to submit the theoretical question if you feel that you
need to improve your final grade. Questions on Slack
channel #home-assignments or/and during the office hours are
welcome.

Take a look at a (partial) analysis example in`examples/analysis`
of the course repository. I expect you to do
as much and hopefully more, for each model, to get full grade.


## Question 1: Analysis of the ride-hailing model

This question is based on the model you developed for Q4 in
Assignment 2. 

1. Check the model. Choose test variables appropriate for the
   problem, represent the results of checks graphically and
   numerically (compute p-values).
2. Suggest an alternative model (different priors, different
   parameterization) and compare the original and the
   alternative model. Use cross-validation for the evaluation.

You are welcome to use either the model you implemented, or the
model in `examples/rides` in the course repository if you had
problems making your implementation work properly, I will grade
either option the same.

## Question 2: Analysis of the algotrading model

1. Check the model. Choose test variables appropriate for the
   problem, represent the results of checks graphically and
   numerically (compute p-values).
2. Suggest an expanded model (with more parameters and possibly
   deeper hierarchy) and compare the original and the
   expanded model. Use WAIC for the evaluation.

## Question 3 (bonus): 6.4 in the book

Model checking and sensitivity analysis: find a published
Bayesian data analysis from the statistical literature.

(a) Compare the data to posterior predictive replications of the data.

(b) Perform a sensitivity analysis by computing posterior
inferences under plausible alter- native models.
